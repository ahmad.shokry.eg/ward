package main

import (
	"net/http"

	"dailyward.lol/controllers/auth"
	"dailyward.lol/controllers/ward"
	"dailyward.lol/middlewares"

	"github.com/gin-gonic/gin"
)

func setupRoutes() {
	// Test base URL
	server.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, "API is working.")
	})

	// Auth
	authRoute := server.Group("/auth")
	authRoute.POST("/register", auth.Register)
	authRoute.POST("/login", auth.Login)
	authRoute.POST("/request-code", auth.RequestCode)
	authRoute.POST("/change-password", auth.ChangePassword)

	// Ward
	wardRoute := server.Group("/ward")
	wardRoute.Use(middlewares.ValidateJWT)
	wardRoute.POST("/create", ward.Create)
	wardRoute.GET("/today", ward.GetToday)

	// Item
	itemRoute := server.Group("/item")
	itemRoute.Use(middlewares.ValidateJWT)
	itemRoute.PUT("/update")
	itemRoute.DELETE("/delete")
}
