package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

var server *gin.Engine

func main() {
	// load the .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Create the gin server
	server = gin.Default()

	// Setup the routes
	setupRoutes()

	// Start the server
	server.Run(":" + os.Getenv("APP_PORT"))
}
