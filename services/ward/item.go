package ward

import "gorm.io/gorm"

type Item interface {
	AddItem(wardID, itemTemplateID uint) error
	GetItems(wardID uint) ([]*item, error)
	UpdateItem(ItemID uint, value string) error
	DeleteItem(ItemID uint) error
}

type item struct {
	gorm.Model
	wardID         uint
	value          string
	itemTemplateID uint
}

func NewItemInterface() Item {
	return &item{}
}

func (i *item) AddItem(wardID, itemTemplateID uint) error {
	// ...
	return nil // TODO:
}

func (i *item) GetItems(wardID uint) ([]*item, error) {
	// ...
	return []*item{}, nil // TODO:
}

func (i *item) UpdateItem(ItemID uint, value string) error {
	// ...
	return nil // TODO:
}

func (i *item) DeleteItem(ItemID uint) error {
	// ...
	return nil // TODO:
}
