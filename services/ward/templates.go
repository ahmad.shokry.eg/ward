package ward

import "gorm.io/gorm"

type itemTemplate struct {
	gorm.Model
	categoryID   uint
	name         string `gorm:"UNIQUE"`
	DefaultValue string
}

type category struct {
	gorm.Model
	name           string `gorm:"UNIQUE"`
	categoryTypeID uint
}

type categoryType struct {
	gorm.Model
	name string `gorm:"UNIQUE"`
}

// create default category types
var (
	NumberedValues = categoryType{name: "Numbers"}
	CheckedValues  = categoryType{name: "Check"}
)

// create default categories
var (
	Prayers      = category{name: "Prayers", categoryTypeID: CheckedValues.ID}
	ExtraPrayers = category{name: "Extra Prayers", categoryTypeID: CheckedValues.ID}
	Quran        = category{name: "Quran", categoryTypeID: NumberedValues.ID}
	Other        = category{name: "Other", categoryTypeID: CheckedValues.ID}
)

// create default item templates
var (
	// Prayers
	Fajr    = itemTemplate{categoryID: Prayers.ID, name: "Fajr", DefaultValue: "0"}
	Zuhr    = itemTemplate{categoryID: Prayers.ID, name: "Zuhr", DefaultValue: "0"}
	Asr     = itemTemplate{categoryID: Prayers.ID, name: "Asr", DefaultValue: "0"}
	Maghrib = itemTemplate{categoryID: Prayers.ID, name: "Maghrib", DefaultValue: "0"}
	Isha    = itemTemplate{categoryID: Prayers.ID, name: "Isha", DefaultValue: "0"}

	// ExtraPrayers
	ExtraFajr    = itemTemplate{categoryID: ExtraPrayers.ID, name: "Extra Fajr", DefaultValue: "0"}
	ExtraZuhr    = itemTemplate{categoryID: ExtraPrayers.ID, name: "Extra Zuhr", DefaultValue: "0"}
	ExtraMaghrib = itemTemplate{categoryID: ExtraPrayers.ID, name: "Extra Maghrib", DefaultValue: "0"}
	ExtraIsha    = itemTemplate{categoryID: ExtraPrayers.ID, name: "Extra Isha", DefaultValue: "0"}
	Duha         = itemTemplate{categoryID: ExtraPrayers.ID, name: "Duha", DefaultValue: "0"}
	SafeWitr     = itemTemplate{categoryID: ExtraPrayers.ID, name: "SafeWitr", DefaultValue: "0"}

	// Quran
	QuranPages = itemTemplate{categoryID: Quran.ID, name: "Quran Pages", DefaultValue: "0"}
	Qiyam      = itemTemplate{categoryID: Quran.ID, name: "Qiyam", DefaultValue: "0"}
	Esteghfar  = itemTemplate{categoryID: Quran.ID, name: "Esteghfar", DefaultValue: "0"}

	// Other
	Sadaqa     = itemTemplate{categoryID: Other.ID, name: "Sadaqa", DefaultValue: "0"}
	SelatRahem = itemTemplate{categoryID: Other.ID, name: "Selat Rahem", DefaultValue: "0"}
	Ganaza     = itemTemplate{categoryID: Other.ID, name: "Ganaza", DefaultValue: "0"}
)
