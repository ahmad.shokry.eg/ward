package ward

import "gorm.io/gorm"

type Ward interface {
	CreateWard(userID uint, date string) error
	GetWard(userID uint, date string) (*ward, error)
}

type ward struct {
	gorm.Model
	userID uint
	date   string
}

func NewWardInterface() Ward {
	return &ward{}
}

func (w *ward) CreateWard(userID uint, date string) error {
	// ...
	return nil // TODO:
}

func (w *ward) GetWard(userID uint, date string) (*ward, error) {
	// ...
	return w, nil // TODO:
}
