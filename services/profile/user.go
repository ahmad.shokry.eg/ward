package profile

import (
	"dailyward.lol/providers"
	"gorm.io/gorm"
)

type User interface {
	CreateUser(email, password string) error
	GetUser(email string) (*user, error)
	UpdatePassword(email, password string) error
}

func NewUserInterface() User {
	return &user{}
}

type user struct {
	gorm.Model
	Email    string `gorm:"UNIQUE"`
	Password string
}

func (u *user) CreateUser(email, password string) error {
	u = &user{
		Email:    email,
		Password: password,
	}
	return providers.ConnectToDB().Create(&u).Error
}

func (u *user) GetUser(email string) (*user, error) {
	err := providers.ConnectToDB().Where("email = ?", email).First(&u).Error
	return u, err
}

func (u *user) UpdatePassword(email, password string) error {
	u = &user{
		Email:    email,
		Password: password,
	}
	return providers.ConnectToDB().Save(&u).Error
}
