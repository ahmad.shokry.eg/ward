package profile

import (
	"dailyward.lol/providers"
	"gorm.io/gorm"
)

type Code interface {
	SaveCode(userID uint, code string) error
	GetCode(userID uint) (string, error)
	RemoveCode(userID uint) error
}

func NewCodeInterface() Code {
	return &code{}
}

type code struct {
	gorm.Model
	userID uint `gorm:"UNIQUE"`
	Code   string
}

func (c *code) SaveCode(userID uint, Code string) error {
	c = &code{
		userID: userID,
		Code:   Code,
	}
	return providers.ConnectToDB().Create(&c).Error
}

func (c *code) GetCode(userID uint) (string, error) {
	err := providers.ConnectToDB().Where("userid = ?", userID).First(&c).Error
	return c.Code, err
}

func (c *code) RemoveCode(userID uint) error {
	return providers.ConnectToDB().Unscoped().Where("userid = ?", userID).Delete(&code{}).Error
}
