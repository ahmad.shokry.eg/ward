package middlewares

import (
	"net/http"

	"dailyward.lol/helpers/jwt"

	"github.com/gin-gonic/gin"
)

func ValidateJWT(c *gin.Context) {
	const BEARER_SCHEMA = "Bearer "
	authHeader := c.GetHeader("Authorization")
	if len(authHeader) < 1 {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"Success": "error",
			"Error":   "please use authorization.",
		})
		return
	}
	token := authHeader[len(BEARER_SCHEMA):]
	_, err := jwt.Validate(token)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"Success": "error",
			"Error":   "invalid token, please log in again.",
		})
	}
	c.Next()
}
