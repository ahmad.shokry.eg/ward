package providers

import (
	"bytes"
	"errors"
	"net/smtp"
	"os"

	"github.com/joho/godotenv"
)

func SendMail(to, message string) error {
	if err := godotenv.Load(); err != nil {
		return errors.New(PROVIDER_ERROR)
	}

	// smtp config
	from := os.Getenv("MAIL_FROM")
	password := os.Getenv("MAIL_PASSWORD")
	smtpHost := os.Getenv("MAIL_HOST")
	smtpPort := os.Getenv("MAIL_PORT")

	// authentication
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Set headers and body
	headers := make(map[string]string)

	headers["Subject"] = "تطبيق حاسب نفسك"
	headers["From"] = from
	headers["To"] = to

	var msg bytes.Buffer
	for k, v := range headers {
		msg.WriteString(k + ": " + v + "\r\n")
	}

	msg.WriteString("\r\n")
	msg.WriteString(message)

	// sending email
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, []string{to}, msg.Bytes())
	if err != nil {
		return errors.New(PROVIDER_ERROR)
	}

	return nil
}
