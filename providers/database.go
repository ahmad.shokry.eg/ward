package providers

import (
	"errors"
	"net/http"
	"os"

	"dailyward.lol/controllers"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ConnectToDB() *gorm.DB {
	var c *gin.Context
	// get database values from env
	err := godotenv.Load()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: errors.New(PROVIDER_ERROR),
			Data:  nil,
		})
		return nil
	}

	// Database values
	dbhost := os.Getenv("DB_HOST")
	dbport := os.Getenv("DB_PORT")
	dbname := os.Getenv("DB_DATABASE")
	dbuser := os.Getenv("DB_USERNAME")
	dbpassword := os.Getenv("DB_PASSWORD")

	// Combine the values
	dsn := dbuser + ":" + dbpassword + "@tcp(" + dbhost + ":" + dbport + ")/" + dbname + "?parseTime=true"

	// Open the connection with the database and keep it open
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: errors.New(PROVIDER_ERROR),
			Data:  nil,
		})
		return nil
	}

	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: errors.New(PROVIDER_ERROR),
			Data:  nil,
		})
		return nil
	}

	return db
}
