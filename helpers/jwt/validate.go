package jwt

import "github.com/golang-jwt/jwt"

func Validate(token string) (*jwt.Token, error) {
	// validate token
	tkn, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		return tokenSigningKey, nil
	})
	return tkn, err
}
