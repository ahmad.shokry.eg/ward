package jwt

import (
	"time"

	"github.com/golang-jwt/jwt"
)

var tokenSigningKey = []byte("DailyWardForAllah")

type claims struct {
	email string
	jwt.StandardClaims
}

func Generate(email string) (token string, err error) {
	// Set expiration to one day
	expirationTime := time.Now().Add(8760 * time.Hour).Unix()
	// set token claims
	claims := claims{
		email: email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime,
		},
	}

	// Generate token
	UnsignedToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// sign the token and return it
	return UnsignedToken.SignedString(tokenSigningKey)
}
