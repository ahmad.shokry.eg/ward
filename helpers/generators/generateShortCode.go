package generators

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	"time"
)

func GenerateShortCode() string {
	// generate new password hash
	byteArray := bytes.Join(
		[][]byte{[]byte(time.Now().GoString())},
		[]byte("crypto&*^$%"),
	) // generate random complex byte array
	longCode := fmt.Sprintf("%x", sha1.Sum(byteArray)) // complex byte hash
	code := longCode[0:7]                              // Make password shorter
	return string(code)
}
