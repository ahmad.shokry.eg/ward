package ward

import "dailyward.lol/services/ward"

var item = ward.NewItemInterface()

type WardFactory struct {
	UserID uint
	Date   string
	WardID uint
	Items  []ward.Item
}

func (f *WardFactory) Create(userID uint, date string) error {
	// ...
	return nil // TODO:
}

func (f *WardFactory) getToday(userID uint, date string) (*WardFactory, error) {
	// ...
	return f, nil // TODO:
}
