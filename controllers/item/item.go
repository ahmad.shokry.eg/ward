package item

import "github.com/gin-gonic/gin"

type item struct {
	userID uint `json:"userID" binding:"required"`
	wardID uint `json:"wardID" binding:"required"`
}

func Update(c *gin.Context) {
	// ...
}

func Delete(c *gin.Context) {
	// ...
}
