package auth

import (
	"errors"
	"net/http"

	"dailyward.lol/controllers"
	"dailyward.lol/helpers/jwt"
	"dailyward.lol/services/profile"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type registerLogin struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,min=8,max=32"`
}

func Register(c *gin.Context) {
	// get data from request
	var data registerLogin
	if err := c.ShouldBindJSON(&data); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// check if email is already in use
	_, err := profile.NewUserInterface().GetUser(data.Email)
	if err == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: errors.New("email is already in use"),
			Data:  nil,
		})
		return
	}

	// hash password
	password, err := bcrypt.GenerateFromPassword([]byte(data.Password), 10)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// create user
	err = profile.NewUserInterface().CreateUser(data.Email, string(password))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// generate JWT
	token, err := jwt.Generate(data.Email)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// retrun JWT
	c.JSON(http.StatusOK, controllers.API{
		Error: nil,
		Data: gin.H{
			"token": token,
		},
	})
}

func Login(c *gin.Context) {
	// get data from request
	var data registerLogin
	if err := c.ShouldBindJSON(&data); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// get user
	user, err := profile.NewUserInterface().GetUser(data.Email)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// compare password
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(data.Password))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// generate JWT
	token, err := jwt.Generate(data.Email)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// retrun JWT
	c.JSON(http.StatusOK, controllers.API{
		Error: nil,
		Data: gin.H{
			"token": token,
		},
	})
}
