package auth

import (
	"net/http"

	"dailyward.lol/controllers"

	"dailyward.lol/helpers/generators"
	"dailyward.lol/providers"
	"dailyward.lol/services/profile"
	"github.com/gin-gonic/gin"
)

type requestCode struct {
	Email string `json:"email" binding:"required,email"`
}

func RequestCode(c *gin.Context) {
	// get data from request
	var data requestCode
	if err := c.ShouldBindJSON(&data); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// check if email exists
	user, err := profile.NewUserInterface().GetUser(data.Email)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// generate code
	code := generators.GenerateShortCode()

	// save code
	if err := profile.NewCodeInterface().SaveCode(user.ID, code); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// send code to email
	message := "Your code is: " + code
	if err := providers.SendMail(data.Email, message); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// return success
	c.JSON(http.StatusOK, controllers.API{
		Error: nil,
		Data:  nil,
	})
}
