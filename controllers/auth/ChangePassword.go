package auth

import (
	"errors"
	"net/http"

	"dailyward.lol/controllers"
	"dailyward.lol/services/profile"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type changePassword struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,min=8,max=32"`
	Code     string `json:"code" binding:"required"`
}

func ChangePassword(c *gin.Context) {
	// get data from request
	var data changePassword
	if err := c.ShouldBindJSON(&data); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// check if user exists
	user, err := profile.NewUserInterface().GetUser(data.Email)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// check if code is valid
	codeService := profile.NewCodeInterface()
	code, err := codeService.GetCode(user.ID)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	if code != data.Code {
		c.AbortWithStatusJSON(http.StatusBadRequest, controllers.API{
			Error: errors.New("invalid code"),
			Data:  nil,
		})
		return
	}

	// hash password
	password, err := bcrypt.GenerateFromPassword([]byte(data.Password), 10)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// change password
	if err := user.UpdatePassword(user.Email, string(password)); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// delete code
	if err := codeService.RemoveCode(user.ID); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, controllers.API{
			Error: err,
			Data:  nil,
		})
		return
	}

	// return success
	c.JSON(http.StatusOK, controllers.API{
		Error: nil,
		Data:  nil,
	})
}
